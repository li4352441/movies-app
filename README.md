# Prerequis

- Composer 2
- PHP 8

# Pour lancer l'application

Depuis un terminal, dans le dossier racine de l'application :

1. composer dump-autoload
2. cd public && php -S localhost:8080

# Remarques

- Pour un projet en équipe (seul c'est bien aussi) et afin de garantir la même version de PHP et de composer entre les developpeurs, j'utilise aussi de la conteneurisation avec Docker (un container php, un composer, un nginx ou apache). Cela peut également permettre d'avoir les mêmes versions sur les environnements de dev et de prod.

- Pour l'exercice, je n'ai pas utilisé de packages PHP, mais dans un cas concret, sans framework et en fonction du besoin, j'aurais peut-être utilisé:

  - symfony/http-foundation pour le workflow des requêtes / réponses
  - symfony/http-client pour les appels API
  - twig/twig pour le moteur de template
  - monolog/monolog pour les logs
  - symfony/dotenv pour les fichiers d'environnement

- J'ai fait le choix de partir sur une architecture MVC.

- L'application est un peu longue à charger. C'est lié aux multiples requêtes pour trouver le réalisateur d'un film... voir si il n'y a des pistes d'amélioration (une autre endpoint dans l'API, du cache, ...).

- Clé TMDB placée dans un fichier ".env" afin de pouvoir la modifier sur les différents environnements si besoin. Elle est suivi dans le git pour l'exercice, sinon je ne l'aurais pas mise.

- TODO : créer une gestion des erreurs car actuellement uniquement happy flow, créer un système de logrotate pour les logs (avec une création dynamique du fichier).
