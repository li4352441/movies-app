<div class="container mt-3">
    <div class="flex row justify-content-between">
        <p class="w-50">Total movies : <?= $metadata['nbResults']; ?></p>
        <p class="w-50 text-right text-end">Page : <?= $metadata['page'] . '/' . $metadata['nbPages']; ?></p>
    </div>
    <hr />
    <ul class="list-group list-group-flush">
        <?php
        foreach ($movies as $movie) {
        ?>
            <li class="list-group-item p-3">
                <h2><?= $movie->getTitle() . ' (' . $movie->getReleaseYear() . ')' ?></h2>
                <p><strong>Directed by <?= $movie->getRealisator()->getName(); ?></strong></p>
                <p><?= $movie->getSynopsis(); ?></p>
            </li>
        <?php
        }
        ?>
    </ul>

    <?php
    if ($metadata['nbPages'] > 1) {
    ?>
        <nav class="flex justify-content-center">
            <ul class="flex justify-content-center pagination">
                <?php
                if ($metadata['page'] > 1) {
                ?>
                    <li class="page-item"><a class="page-link" href="/?search_title=<?= $parameters['search_title'] . '&page=' . $metadata['page'] - 1; ?>">Previous</a></li>
                <?php
                }
                for ($i = 1; $i <= $metadata['nbPages']; $i++) {
                ?>
                    <li class="page-item"><a class="page-link" href="/?search_title=<?= $parameters['search_title'] . '&page=' . $i; ?>"><?= $i ?></a></li>
                <?php
                }
                if ($metadata['page'] < $metadata['nbPages']) {
                ?>
                    <li class="page-item"><a class="page-link" href="/?search_title=<?= $parameters['search_title'] . '&page=' . $metadata['page'] + 1; ?>">Next</a></li>
                <?php
                }
                ?>
            </ul>
        </nav>
    <?php
    }
    ?>
</div>