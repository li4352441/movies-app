<!DOCTYPE html>
<html lang="en">

<head>
    <title>Movies App</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Retrieve information about a movie">
    <link rel="stylesheet" href="/dist/bootstrap.min.css">
    <link rel="icon" type="image/png" href="/dist/favicon.png">
</head>

<body class="m-2 mt-5">
    <div class="container d-flex flex-column justify-content-center">
        <h1 class="text-center mb-3">Movies App</h1>
        <form class="row g-3 justify-content-center">
            <div class="col-auto">
                <input type="text" class="form-control" name="search_title" placeholder="Search by movie title..." value="<?= $parameters['search_title'] ?>" />
            </div>
            <div class="col-auto">
                <button type="submit" class="btn btn-primary">Search</button>
            </div>
        </form>
    </div>

    <?php
    if ($metadata['nbResults'] > 0) {
        require_once('../src/View/inc/results.html.php');
    } else if ($parameters['search_title'] != '' && $metadata['nbResults'] === 0) {
        require_once('../src/View/inc/no-result.html.php');
    }
    ?>

</body>

</html>