<?php

namespace App\Service;

interface Loggerinterface
{
    public function write(string $text): void;
}
