<?php

namespace App\Service;

use App\Service\LoggerInterface;

class FileLogger implements LoggerInterface
{
    const LOG_FILE = __DIR__ . '../../../private/search.log';

    public function write(string $text): void
    {
        $date = (new \DateTimeImmutable('now'))->format('Y-m-d H:i:s');
        file_put_contents(self::LOG_FILE, "\n$date : $text", FILE_APPEND);
    }
}
