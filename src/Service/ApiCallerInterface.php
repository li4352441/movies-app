<?php

namespace App\Service;

interface ApiCallerInterface
{
    public function call();
}
