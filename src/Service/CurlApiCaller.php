<?php

namespace App\Service;

use App\Service\ApiCallerInterface;

class CurlApiCaller implements ApiCallerInterface
{
    private $curlChannel;

    public function __construct($url)
    {
        $this->curlChannel = curl_init($url);
    }

    public function __destruct()
    {
        curl_close($this->curlChannel);
    }

    public function call(): mixed
    {
        curl_setopt($this->curlChannel, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($this->curlChannel);

        return $result;
    }
}
