<?php

namespace App\Model;

use App\Model\Realisator;

class Movie
{
    private $title;
    private $realisator;
    private $synopsis;
    private $releaseDate;

    /**
     * Get the value of title
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * Set the value of title
     *
     * @return  self
     */
    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get the value of realisator
     */
    public function getRealisator(): Realisator
    {
        return $this->realisator;
    }

    /**
     * Set the value of realisator
     *
     * @return  self
     */
    public function setRealisator(Realisator $realisator): self
    {
        $this->realisator = $realisator;

        return $this;
    }

    /**
     * Get the value of synopsis
     */
    public function getSynopsis(): string
    {
        return $this->synopsis;
    }

    /**
     * Set the value of synopsis
     *
     * @return  self
     */
    public function setSynopsis(string $synopsis): self
    {
        $this->synopsis = $synopsis;

        return $this;
    }

    /**
     * Get the value of releaseDate
     */
    public function getReleaseDate(): \DateTimeImmutable
    {
        return $this->releaseDate;
    }

    /**
     * Set the value of releaseDate
     *
     * @return  self
     */
    public function setReleaseDate(\DateTimeImmutable $releaseDate): self
    {
        $this->releaseDate = $releaseDate;

        return $this;
    }

    /**
     * Get the value of releaseYear
     */
    public function getReleaseYear(): int
    {
        return $this->releaseDate->format('Y');
    }
}
