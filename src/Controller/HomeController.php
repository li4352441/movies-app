<?php

namespace App\Controller;

use App\AbstractController;
use App\Repository\MovieRepository;
use App\Repository\RealisatorRepository;
use App\Model\Movie;
use App\Model\Realisator;
use App\Service\FileLogger;

class HomeController extends AbstractController
{
    function displayHome()
    {
        if (isset($_GET['search_title'])) {
            $movieRepository = new MovieRepository();
            $movies = $movieRepository->getByTitle($_GET['search_title'], $_GET['page'] ?? 1);

            $moviesCollection = [];
            foreach ($movies['results'] as $movie) {
                $realisatorRepository = new RealisatorRepository();
                $realisatorName = $realisatorRepository->getByMovieId($movie['id']);

                $realisator = (new Realisator())->setName($realisatorName);

                $moviesCollection[] = (new Movie())
                    ->setTitle($movie['title'])
                    ->setSynopsis($movie['overview'])
                    ->setRealisator($realisator)
                    ->setReleaseDate(new \DateTimeImmutable($movie['release_date']));
            }

            $metadata = ['nbResults' => $movies['total_results'], 'nbPages' => $movies['total_pages'], 'page' => $movies['page']];

            $fileLogger = new FileLogger();
            $fileLogger->write($_GET['search_title']);
        }


        $this->render('home', array_merge(['parameters' => $_GET], ['movies' => $moviesCollection], ['metadata' => $metadata]));
    }
}
