<?php

namespace App\Repository;

use App\Repository\Repository;
use App\Service\CurlApiCaller;

final class MovieRepository extends Repository
{
    const TMDB_MOVIES_ENDPOINT = parent::TMDB_URL . '3/search/movie';

    public function __construct()
    {
        parent::__construct();
    }

    public function getByTitle(string $title, int $page = 1): array
    {
        $curlApiCaller = new CurlApiCaller(self::TMDB_MOVIES_ENDPOINT . '?query=' . urlencode($title) . '&page=' . $page . '&api_key=' . getenv('TMDB_API_KEY'));
        $movies = $curlApiCaller->call();

        return json_decode($movies, true);
    }
}
