<?php

namespace App\Repository;

use App\Repository\Repository;
use App\Service\CurlApiCaller;

final class RealisatorRepository extends Repository
{
    const TMDB_MOVIES_ENDPOINT = parent::TMDB_URL . '3/movie/{movie_id}/credits';
    const TMDB_REALISTOR_PROPERTY_NAME = 'Director';

    public function __construct()
    {
        parent::__construct();
    }

    public function getByMovieId(string $movieId): string
    {
        $curlApiCaller = new CurlApiCaller(str_replace('{movie_id}', $movieId, self::TMDB_MOVIES_ENDPOINT . '?api_key=' . getenv('TMDB_API_KEY')));
        $credits = $curlApiCaller->call();

        $credits = json_decode($credits, true);
        $crew = $credits["crew"];

        if (isset($crew)) {
            foreach ($crew as $member) {
                foreach ($member as $key => $value) {
                    if ($key ===  "job" && $value === self::TMDB_REALISTOR_PROPERTY_NAME) {
                        return $member["name"];
                    }
                }
            }
        }

        return 'Unknown';
    }
}
