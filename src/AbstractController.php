<?php

namespace App;

abstract class AbstractController
{
    protected function render($view, $data = [])
    {
        extract($data);

        require_once "View/$view.html.php";
    }
}
