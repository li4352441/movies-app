<?php

use App\Controller\HomeController;
use App\DotEnv;

require_once dirname(__DIR__) . '/vendor/autoload.php';
(new DotEnv)->load(__DIR__ . '/../');

$homeController = new HomeController();
$homeController->displayHome();
